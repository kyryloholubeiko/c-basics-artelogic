﻿using FilesSizeList;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace UnitTestsList
{
    [TestClass]
    public class ListTests
    {
        FileSizeList fileSizeList = null;

        [TestMethod]
        public void TestCreatingFile()
        {
            fileSizeList = new FileSizeList("TestFolderWithFiles");

            fileSizeList.GetSortedFilesList();
            fileSizeList.PrintList();
            fileSizeList.SaveList();

            var strContent = string.Empty;

            string filePath3 = Path.Combine(fileSizeList.path, "File3.txt");

            using (StreamReader sr = new StreamReader(filePath3))
            {
                strContent = sr.ReadToEnd();
            }
            Assert.AreEqual(strContent, "16 - bit Windowstes were released in the two following years adding support for more hardware..");

        }
    }
}
