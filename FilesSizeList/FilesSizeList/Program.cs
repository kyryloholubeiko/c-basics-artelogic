﻿using System;
using System.IO;

namespace FilesSizeList
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSizeList fileSizeList = new FileSizeList("TestFolderWithFiles");
            fileSizeList.GetFilesList();
            fileSizeList.PrintList();

            Console.WriteLine("------------------------------------------");

            fileSizeList.GetSortedFilesList();
            fileSizeList.PrintList();
            fileSizeList.SaveList();
        }
    }
}
