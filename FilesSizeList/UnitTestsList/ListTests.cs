﻿using FilesSizeListLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UnitTestsList
{
    [TestClass]
    public class ListTests
    {
        FileSizeList fileSizeList = null;

        //<summary>
            //this test checks one of the files for creation at the root of the project
        //<summary>

        [TestMethod]
        public void TestCreatingFile()
        {
            fileSizeList = new FileSizeList("TestFolderWithFiles");

            fileSizeList.GetSortedFilesList();

            var strContent = string.Empty;

            string filePath3 = Path.Combine(fileSizeList.path, "File3.txt");

            using (StreamReader sr = new StreamReader(filePath3))
            {
                strContent = sr.ReadToEnd();
            }
            Assert.AreEqual(strContent, "16-bit Windowstes were released in the two following years adding support for more hardware..");
        }

        //<summary>
             //this test checks the created file with List values
        //<summary>

        [TestMethod]
        public void TestSavingListToFile()
        {
            fileSizeList = new FileSizeList("TestFolderWithFiles");
            fileSizeList.GetSortedFilesList();
            fileSizeList.SaveList();

            var strContent = string.Empty;

            using (StreamReader sr = new StreamReader("List.txt"))
            {
                strContent = sr.ReadToEnd();
            }
            Assert.AreEqual(strContent, $"File2.txt | 2 KB | {DateTime.Now} File1.txt | 0 KB | {DateTime.Now} File3.txt | 0 KB | {DateTime.Now} ");
        }

        //<summary>
            //this test checks if the values ​​in the list are equal
        //<summary>

        [TestMethod]
        public void TestGetFilesList()
        {
            fileSizeList = new FileSizeList("TestFolderWithFiles");
            fileSizeList.GetFilesList();

            List<string> testlist = new List<string>();
            testlist.Add("File1.txt");
            testlist.Add("File2.txt");
            testlist.Add("File3.txt");

            bool isEqual = testlist.SequenceEqual(fileSizeList.list);

            Assert.IsTrue(isEqual, "true");
        }

        //<summary>
            //this test checks if the values ​​in the sorted list are equal
        //<summary>

        [TestMethod]
        public void CheckSortedList()
        {
            fileSizeList = new FileSizeList("TestFolderWithFiles");
            fileSizeList.GetSortedFilesList();

            List<string> testlist = new List<string>();
            testlist.Add($"File2.txt | 2 KB | {DateTime.Now}");
            testlist.Add($"File1.txt | 0 KB | {DateTime.Now}");
            testlist.Add($"File3.txt | 0 KB | {DateTime.Now}");

            bool isEqual = testlist.SequenceEqual(fileSizeList.list);

            Assert.IsTrue(isEqual, "true");
        }
    }
}
