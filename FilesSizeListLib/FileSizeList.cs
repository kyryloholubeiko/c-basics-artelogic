﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilesSizeListLib
{
    public class FileSizeList
    {
        public List<string> list { get; set; }
        DirectoryInfo di;
        FileInfo[] files;

        public string path { get; set; }

        public FileSizeList(string path)
        {
            this.path = path;

            di = new DirectoryInfo(path);
            if (!di.Exists)
                di.Create();

            string filePath1 = Path.Combine(path, "File1.txt");
            string filePath2 = Path.Combine(path, "File2.txt");
            string filePath3 = Path.Combine(path, "File3.txt");

            using (StreamWriter writer = new StreamWriter(filePath1))
            {
                writer.Write("16-bit Windows\nWindows 1.01, the first version of Windows released into retail Microsoft Windows debuted to the world during the Fall COMDEX 1983 computer expo as an operating environment running on top MS - DOS." +
                    "The final version of the product with the version number of 1.01 was later released on 20 November 1985 and did not gain much popularity.Windows 1.0 was a cooperative multitasking desktop environment with a tiling window manager." +
                    "The first versions of Windows used the MS - DOS Executive, which was a simple file manager, as a shell, which is generally the first application ran on startup providing the user experience." +
                    "Other applications included in the first version of Windows included Calculator, Cardfile, Clipboard Viewer, Clock, Control Panel, Notepad, Paint, Reversi, Spooler, Terminal, and Write." +
                    "Three minor updates were released in the two following years adding support for more hardware.");
            }
            using (StreamWriter writer = new StreamWriter(filePath2))
            {
                writer.Write("Create the wiki home page When a wiki is created, it is empty.On your first visit, " +
                    "create the landing page users see when viewing the wiki:  Go to your project or group and select Wiki. \n" +
                    "Select Create your first page. Select a Format for styling your text. \n" +
                    "Add a welcome message in the Content section.You can always edit it later. \n" +
                    "Add a Commit message.Git requires a commit message, so GitLab creates one if you don’t enter one yourself. Select Create page. \n" +
                    "Create a new wiki page Users with Developer permissions can create new wiki pages:Go to your project or group and select Wiki. \n" +
                    "Select New page on this page, or any other wiki page. Select a content format. Add a title for your new page.\n" +
                    "Page titles use special characters for subdirectories and formatting, and have length restrictions. \n" +
                    "Add content to your wiki page. \n" +
                    "(Optional) Attach a file, and GitLab stores it according to your installed version of GitLab: Files added in GitLab 11.3 and later: Files are stored in the wiki’s Git repository. \n" +
                    "Files added GitLab 11.2 and earlier: Files are stored in GitLab itself.To add the file to the wiki’s Git repository, you must re - upload the file. Add a Commit message.\n" +
                    "Git requires a commit message, so GitLab creates one if you don’t enter one yourself. \n" +
                    "Select Create page. \n" +
                    "Create or edit wiki pages locally Wikis are based on Git repositories, so you can clone them locally and edit them like you would do with every other Git repository. \n" +
                    "To clone a wiki repository locally, select Clone repository from the right-hand sidebar of any wiki page, and follow the on-screen instructions. \n" +
                    "Files you add to your wiki locally must use one of the following supported extensions, depending on the markup language you wish to use.Files with unsupported extensions don’t display when pushed to GitLab: Markdown extensions: .mdown, .mkd, .mkdn, .md, .markdown. \n" +
                    "AsciiDoc extensions: .adoc, .ad, .asciidoc. Other markup extensions: .textile, .rdoc, .org, .creole, .wiki, .mediawiki, .rst. Special characters in page titles Wiki pages are stored as files in a Git repository, so certain characters have a special meaning: Spaces are converted into hyphens when storing a page. Hyphens(-) are converted back into spaces when displaying a page. " +
                    "Slashes(/) are used as path separators, and can’t be displayed in titles.\n" +
                    "If you create a title containing / characters, GitLab creates all the subdirectories needed to build that path. \n" +
                    "For example, a title of docs / my - page creates a wiki page with a path / wikis / docs / my - page. Length restrictions for file and directory names\n");
            }
            using (StreamWriter writer = new StreamWriter(filePath3))
            {
                writer.Write("16-bit Windowstes were released in the two following years adding support for more hardware..");
            }
        }

        public void GetFilesList()
        {
            Console.WriteLine($"Original files in {AppDomain.CurrentDomain.BaseDirectory}{path}");
            list = new List<string>();

            di = new DirectoryInfo(path);
            files = di.GetFiles();

            foreach (FileInfo file in files)
            {
                list.Add($"{file.Name}");
            }
        }

        public void GetSortedFilesList()
        {
            Console.WriteLine("Sorted by lenght");
            list = new List<string>();

            di = new DirectoryInfo(path);
            files = di.GetFiles();

            foreach (FileInfo file in files.OrderByDescending(x => x.Length / 1024))
            {
                list.Add($"{file.Name} | {file.Length / 1024} KB | {file.LastWriteTime}");
            }
        }

        public void SaveList()
        {
            using (StreamWriter sw = new StreamWriter("List.txt", false, System.Text.Encoding.Default))
            {
                foreach (var item in list)
                {
                    sw.Write(item + " ");
                }
            }
        }

        public void PrintList()
        {
            foreach (var item in list)
            {
                Console.WriteLine($"{item}");
            }
        }
    }
}
